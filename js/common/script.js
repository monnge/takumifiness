$(function() {
  //preloadImages();
  var hash=location.hash.replace("#","");
  if(hash !== null)refreshNavFocus(hash,0);

  $(document).on('click',"#nav dl dd a",function(){
    var hash=$(this).attr("href").split('#')[1];
    refreshNavFocus(hash,300);
  });
});
/*function preloadImages()
{
  var target=$('#nav dl dd a img');
  target.each(function(){
    var src=$(this).attr("src").replace("_inactive","_active");
    $("<img>").attr("src",src);
  });
}*/
function refreshNavFocus(hash,delay)
{
  setTimeout(function(){
    var target=$('#nav dl dd');
    target.each(function(){
      var img=$(this).find("img");
      var src=img.attr("src");
      if(src.indexOf("_active") != -1)
      {
        //アクティブな画像をインアクティブにする
        img.attr("src",src.replace("_active","_inactive"));
      }

      if($(this).attr("ref") === hash)
      {
        //画像をアクティブにする
        img.attr("src",src.replace("_inactive","_active"));
        return;
      }
    });
  },delay);
}